# Reduce, Delegate, Visualize (RDViz) Version 3

## Introduction
This repository contains the source code of the RDViz project.

## Configuration
There is no extra configuration needed. You can download this repository via ZIP or clone it
and start the Application by running the index.html in your browser.

## Used Technology
The main functionality of the application is realized using javascript, jquery and svg for drawing.
Bootstrap and css are used for the layout and design of the application

## Structure
### css
Contains css for page layout

### img
Contains images

### js
Contains the main javascript file